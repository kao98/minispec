import assert from 'assert/strict'
import { exec } from 'child_process'
import util from 'util'
import fs from 'fs'

import MiniSpec, { context, describe, it } from '../src/index.js'
import { BaseReporter, SummaryReporter, ConsoleReporter, JunitReporter } from '../src/reporters/index.js'

type loader = {
  name: string,
  extension: string,
  command: string
}

describe('MiniSpec acceptance tests', async () => {
  const run = util.promisify(exec)

  const loaders: loader[] = [
    { name: 'cjs', extension: 'cjs', command: 'node' },
    { name: 'esm', extension: 'mjs', command: 'node' },
    { name: 'ts', extension: 'ts', command: 'node --loader ts-node/esm --no-warnings' }
  ]

  Promise.all(loaders.map(async (loader) => {
    describe(`using ${loader.name}`, async () => {
      const folder = `./acceptance/${loader.name}`

      context('when passing', async () => {
        const passingExample = `${folder}/passing/minispec_entrypoint.${loader.extension}`

        it('exit with no error', async () => {
          try {
            await run(`${loader.command} ${passingExample}`)
          } catch (_) {
            assert.fail()
          }
        })

        it('shows the report in stdout', async () => {
          const { stdout } = await run(`${loader.command} ${passingExample}`)

          assert.ok(stdout.includes('MiniSpec\n'))
          assert.ok(stdout.includes('  is working'))
          assert.ok(stdout.includes('Finished in'))
          assert.ok(stdout.includes('1 test, no failure 👏'))
        })
      })

      context('when failing', async () => {
        const failingExample = `${folder}/failing/minispec_entrypoint.${loader.extension}`

        it('exit with an error', async () => {
          try {
            await run(`${loader.command} ${failingExample}`)
          } catch (error) {
            assert.ok(error.code)
            return
          }

          assert.fail()
        })

        it('shows the errors in stderr', async () => {
          try {
            await run(`${loader.command} ${failingExample}`)
          } catch (error) {
            assert.ok(error.stdout.includes('MiniSpec\n'))
            assert.ok(error.stdout.includes('Finished in'))
            assert.ok(error.stdout.includes('1 test, 1 failure'))

            assert.ok(error.stderr.includes('Failures:'))
            assert.ok(error.stderr.includes('MiniSpec, it is failing'))
          }
        })
      })
    })
  }))
})

const reporters: BaseReporter[] = []

if (process.env['CI']) {
  const junitReport = fs.createWriteStream('./junitAcceptanceReport.xml')

  reporters.push(new SummaryReporter())
  reporters.push(new JunitReporter(junitReport))
} else {
  reporters.push(new ConsoleReporter())
}

MiniSpec
  .getInstance()
  .setReporters(reporters)
  .execute()
