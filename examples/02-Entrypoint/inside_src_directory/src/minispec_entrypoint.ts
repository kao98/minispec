import MiniSpec from 'minispec'

import './index_spec'
import './lib_spec'

MiniSpec.execute()
