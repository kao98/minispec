import * as assert from 'assert'
import { describe, it } from 'minispec'
import { lib } from './lib'

describe('lib', async () => {
  it('is the lib', async () => {
    assert.equal(lib, 'lib')
  })
})
