import * as assert from 'assert'
import { describe, it } from 'minispec'
import { index } from '../src/index'

describe('index', async () => {
  it('is the index', async () => {
    assert.equal(index, 'index')
  })
})
