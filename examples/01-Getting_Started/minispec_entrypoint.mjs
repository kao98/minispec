import assert from 'assert/strict'

import MiniSpec from 'minispec'
import { describe, it } from 'minispec'

describe('MiniSpec', async () => {
  it('is working', async () => {
    const result = 'MiniSpec'

    assert.equal(result, 'MiniSpec')
  })
})

MiniSpec.execute()
