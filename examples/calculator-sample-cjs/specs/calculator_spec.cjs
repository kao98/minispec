const assert = require('assert').strict // eslint-disable-line

const { describe, context, beforeEach, it } = require('minispec') // eslint-disable-line
const Calculator = require('../src/calculator.cjs').default // eslint-disable-line

describe('Calculator', async () => {
  let calculator

  beforeEach(async () => {
    calculator = new Calculator()
  })

  describe('.sum(a, b)', async () => {
    context('when a and b are valid numbers', async () => {
      const a = 40
      const b = 2

      it('returns the sum of a and b', async () => {
        assert.equal(calculator.sum(a, b), 42)
      })
    })
  })
})
