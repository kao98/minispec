import { expect } from 'chai'

import MiniSpec from '../../../dist/esm/index.js'
import { describe, it } from '../../../dist/esm/index.js'

describe('MiniSpec', async () => {
  it('is working well with chai', async () => {
    expect(1).to.equal(1)
  })
})

MiniSpec.execute()
