import { expect } from 'chai'

import MiniSpec from '../../../dist/esm/index.js'
import { describe, it } from '../../../dist/esm/index.js'

describe('MiniSpec', async () => {
  it('is failing as expected with chai', async () => {
    expect(1).not.to.equal(1)
  })
})

MiniSpec.execute()
