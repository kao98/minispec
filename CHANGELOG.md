
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

## [1.3.0] - 2024-09-19

### Added

- Official support for Chai Assertion Library: while Chai was already
  working fine with MiniSpec, its support is now enforced via automated
  integration tests. That means also that if you are facing any issue
  using Chai with MiniSpec, you can report it and we'll do our best
  to fix it.
- Support for Node.js v22

### Changed

- [Internal] Bump development dependencies
- [Internal] Usage of npm workspaces to manage examples and integration tests
- Bump dependencies from the various examples

### Deprecated

- Support for Node.js v16 is deprecated and will be dropped with next major
  version of MiniSpec: v2.0

## [1.2.0] - 2024-03-19

### Added

- Support for negated assertion
  ([MR#10](https://gitlab.com/kao98/minispec/-/merge_requests/10))

- New Real-life example: [examples/calculator-sample](https://gitlab.com/kao98/minispec/-/tree/main/examples/calculator-sample?ref_type=heads). It replicates the example from the README.

## [1.1.4] - 2023-12-21

- Improving README

### Added

- [Internal] Enforcing support for node 16, 18 and 20 through CI

## [1.1.3]

### Changed

- Removing legacy files from the built package. As those files were part of the
  private API, this is not a breaking change.

## [1.1.2]

This is yet another tiny maintenance release, yet brining a little bit more
documentation to the public API.

### Added

- TSDoc documentation to `ignores`, `skip` and `focus` methods

### Changed

- [Internal] Bump development dependencies
- [Internal] Rename the class `ContextManager` into `ActiveContext`

## [1.1.1]

This is a maintenance release. It improves a little bit the internals thanks to
the addition of eslint. It is also the first iteration in documenting the public
API using TSDoc.

### Added

- TSDoc documentation to the public API

## [1.1.0]

### Added

- Basic benchmarking capabilities to junit reporter: it now reports timestamp for
  `testsuite` node, and testcase duration in seconds in the `time` attribute of
  a `testcase` node.

## [1.0.0]

This is the first stable version for MiniSpec \o/

It does bring very little changes, a lot related to the documentation.

## [1.0.0-beta.1]

### Added

- Basic junit reporter
  ([MR#2](https://gitlab.com/kao98/minispec/-/merge_requests/3))

- Methods to skip tests and context:
  - `sit`, `it.s` and `it.skip`
  - `scontext`, `context.s`, and `context.skip`
  - `sdescribe`, `describe.s`, `describe.skip`

### Changed

- The license has been changed to the MIT license.

- The SummaryReporter now shows location of failed tests rather than the failed
  assertion in the section 'Spec with failures'. The failures still shows the
  location of the failed assertion.
  ([MR#2](https://gitlab.com/kao98/minispec/-/merge_requests/2))

- It is not possible to ignores or focus tests/contexts/describe blocks when a
  CI is detected.

## [v1.0.0-alpha.3] - 2021-11-25

### Fixed

- The process exit code was not good anymore in case of failure

## [v1.0.0-alpha.2] - 2021-00-05

### Added

- Methods to give focus on tests and context
  - `fit`, `it.f` and `it.focus`
  - `fcontext`, `context.f` and `context.focus`
  - `fdescribe`, `describe.f` and `describe.focus`

- Methods to ignore tests and context
  - `xit`, `it.x` and `it.ignores`
  - `xcontext`, `context.x` and `context.ignores`
  - `xdescribe`, `describe.x` and `describe.ignores`

- Report the number of executed tests with reporters
- A SummaryReporter, now capable of basic benchmarking
- A ProgressReporter
- A BenchmarkReporter: reports execution duration of execution of tests and contexts.
  Hooks are not reported yet, that may arrive in a further version depending on the
  demand.

### Changed

- Use streams with reporters rather than the `console` object.
  ([MR#1](https://gitlab.com/kao98/minispec/-/merge_requests/1))

### Fixed

- The console reporter reports failed files and lines more accurately

## [v1.0.0-alpha.1] - 2021-07-29

Initial release of a first alpha

[1.3.0]: https://gitlab.com/kao98/minispec/-/releases/v1.3.0
[1.2.0]: https://gitlab.com/kao98/minispec/-/releases/v1.2.0
[1.1.4]: https://gitlab.com/kao98/minispec/-/releases/v1.1.4
[1.1.3]: https://gitlab.com/kao98/minispec/-/releases/v1.1.3
[1.1.2]: https://gitlab.com/kao98/minispec/-/releases/v1.1.2
[1.1.1]: https://gitlab.com/kao98/minispec/-/releases/v1.1.1
[1.1.0]: https://gitlab.com/kao98/minispec/-/releases/v1.1.0
[1.0.0]: https://gitlab.com/kao98/minispec/-/releases/v1.0.0
[1.0.0-beta.1]: https://gitlab.com/kao98/minispec/-/tags/v1.0.0-beta.1
[v1.0.0-alpha.3]: https://gitlab.com/kao98/minispec/-/tags/v1.0.0-alpha.3
[v1.0.0-alpha.2]: https://gitlab.com/kao98/minispec/-/tags/v1.0.0-alpha.2
[v1.0.0-alpha.1]: https://gitlab.com/kao98/minispec/-/tags/v1.0.0-alpha.1
