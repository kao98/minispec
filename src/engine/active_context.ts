import Context from '../runnables/context.js'
import { IEngine } from './engine.js'

export default class ActiveContext {
  private context: Context | null = null

  constructor(private readonly engine: IEngine) {}

  public get(): Context {
    if (this.context === null) {
      this.context = new Context('', async () => { /* no-op */ }, { engine: this.engine })
    }

    return this.context
  }

  public set(value: Context) {
    this.context = value
  }

  public hasOne(): boolean {
    return this.context !== null
  }
}
