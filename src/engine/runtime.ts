import { IEngine } from './engine.js'
import ActiveContext from './active_context.js'
import Context from '../runnables/context.js'
import Test from '../runnables/test.js'
import Hook from '../runnables/hook.js'

interface IAddTestOptions {
  focused?: boolean,
  skipped?: boolean,
  location?: string,
}

interface IAddContextOptions {
  focused?: boolean,
  skipped?: boolean,
}

export default class Runtime {
  private contexts: Context[] = []
  private hasSomeFocus = false

  constructor(
    private readonly engine: IEngine,
    private readonly activeContext: ActiveContext
  ) { }

  public async discover(): Promise<void> {
    for (const context of this.contexts) {
      await context.discover()
    }
  }

  public async execute(): Promise<void> {
    for (const context of this.contexts) {
      await context.execute(this.hasSomeFocus)
    }
  }

  public addContext(name: string, code: () => Promise<void>, options?: IAddContextOptions): void {
    const context = new Context(
      name,
      code,
      {
        focused: options?.focused || false,
        skipped: options?.skipped || false,
        engine: this.engine,
      }
    )

    this.hasSomeFocus = this.hasSomeFocus || options?.focused || false

    if (this.activeContext.hasOne()) {
      this.activeContext.get().addChildContext(context)
      return
    }

    this.contexts.push(context)
  }

  public addTest(name: string, code: () => Promise<void>, options?: IAddTestOptions): void {
    const test = new Test(
      name,
      code,
      {
        engine: this.engine,
        skipped: options?.skipped || false,
        location: options?.location || undefined
      }
    )

    this.activeContext.get().addTest(test, options?.focused || false)
    this.hasSomeFocus = this.hasSomeFocus || options?.focused || false
  }

  public addBeforeAllHook(code: () => Promise<void>): void {
    const hook = new Hook(code)

    this.activeContext.get().addBeforeAllHook(hook)
  }

  public addAfterAllHook(code: () => Promise<void>): void {
    const hook = new Hook(code)

    this.activeContext.get().addAfterAllHook(hook)
  }

  public addBeforeEachHook(code: () => Promise<void>): void {
    const hook = new Hook(code)

    this.activeContext.get().addBeforeEachHook(hook)
  }

  public addAfterEachHook(code: () => Promise<void>): void {
    const hook = new Hook(code)

    this.activeContext.get().addAfterEachHook(hook)
  }
}
