
import Runtime from './runtime.js'
import { IReporter, ConsoleReporter } from '../reporters/index.js'
import Context from '../runnables/context.js'
import Test from '../runnables/test.js'
import ActiveContext from './active_context.js'

export interface IEngine {
  activeContext: ActiveContext

  discover(): Promise<IEngine>
  execute(): Promise<void>
  addReporter(reporter: IReporter): IEngine
  addReporters(reporters: IReporter | IReporter[]): IEngine
  setReporter(reporter: IReporter): IEngine
  setReporters(reporters: IReporter | IReporter[]): IEngine
  reportContextStarted(context: Context): void
  reportContextFinished(): void
  reportTestStarted(test: Test): void
  reportTestFinished(): void
  reportTestFailed(error: Error): void
}

export default class Engine implements IEngine {
  private static instance: Engine

  public readonly runtime: Runtime
  public readonly activeContext: ActiveContext
  private reporters: IReporter[] = []
  private exitcode = 0
  private discovered = false

  private constructor(
    reporters?: IReporter | IReporter[]
  ) {
    this.activeContext = new ActiveContext(this)
    this.runtime = new Runtime(this, this.activeContext)
    this.addReporters(reporters || new ConsoleReporter)
  }

  public static getInstance(): Engine {
    if (this.instance) {
      return this.instance
    }

    return this.instance = new Engine()
  }

  public static async discover(): Promise<IEngine> {
    return this.getInstance().discover()
  }

  public static async execute(): Promise<void> {
    await this.getInstance().execute()
  }

  public async discover(): Promise<IEngine> {
    await this.runtime.discover()

    this.discovered = true

    return this
  }

  public async execute(): Promise<void> {
    for (const reporter of this.reporters) {
      reporter.startTimer(Date.now())
    }

    if (!this.discovered) {
      await this.discover()
    }

    for (const reporter of this.reporters) {
      reporter.addTimerInterval(Date.now(), 'discovering')
    }

    await this.runtime.execute()

    for (const reporter of this.reporters) {
      reporter.addTimerInterval(Date.now(), 'execution')
      reporter.stopTimer(Date.now())
    }

    for (const reporter of this.reporters) {
      reporter.summarizeExecution()
    }

    process.exitCode = this.exitcode
  }

  public addReporter(reporter: IReporter): IEngine {
    this.addReporters(reporter)

    return this
  }

  public addReporters(reporters: IReporter | IReporter[]): IEngine {
    if (Array.isArray(reporters)) {
      this.reporters.push(...reporters)
    } else {
      this.reporters.push(reporters)
    }

    return this
  }

  public setReporter(reporter: IReporter): IEngine {
    this.setReporters(reporter)

    return this
  }

  public setReporters(reporters: IReporter | IReporter[]): IEngine {
    this.reporters = []

    if (Array.isArray(reporters)) {
      this.reporters.push(...reporters)
    } else {
      this.reporters.push(reporters)
    }

    return this
  }

  public reportContextStarted(context: Context): void {
    for (const reporter of this.reporters) {
      reporter.startContext(context)
    }
  }

  public reportContextFinished(): void {
    for (const reporter of this.reporters) {
      reporter.stopContext()
    }
  }

  public reportTestStarted(test: Test): void {
    for (const reporter of this.reporters) {
      reporter.startTest(test)
    }
  }

  public reportTestFinished(): void {
    for (const reporter of this.reporters) {
      reporter.stopTest()
    }
  }

  public reportTestFailed(error: Error): void {
    if (error.message !== 'skipped') {
      this.exitcode = 1
    }

    for (const reporter of this.reporters) {
      reporter.stopTest(error)
    }
  }
}
