import Context from '../runnables/context.js'
import Test from '../runnables/test.js'

export default interface IReporter {
  startContext(context: Context): void
  stopContext(): void
  startTest(test: Test): void
  stopTest(error?: Error): void
  summarizeExecution(): void
  startTimer(timeInMilliseconds?: number): void
  addTimerInterval(timeInMilliseconds?: number, intervalName?: string): void
  stopTimer(timeInMilliseconds?: number): void
}
