import IReporter from './ireporter.js'
import Benchmarker from '../services/benchmarker.js'
import ConsoleReporter from './console_reporter.js'
import Test from '../runnables/test.js'
import Context from '../runnables/context.js'
import { Writable } from 'stream'

export default class BenchmarkReporter extends ConsoleReporter {
  private contextBenchmarkers: Benchmarker[] = []

  constructor(
    summaryReporter?: IReporter,
    outStream?: Writable,
    errorStream?: Writable,
    protected readonly testBenchmarker: Benchmarker = new Benchmarker(),
    protected readonly contextBenchmarkerFactory: () => Benchmarker = () => new Benchmarker()
  ) {
    super(summaryReporter, outStream, errorStream)
  }

  public startContext(context: Context): void {
    const benchmarker = this.contextBenchmarkerFactory()

    benchmarker.start()
    this.contextBenchmarkers.push(benchmarker)

    super.startContext(context)
  }

  public stopContext(): void {
    if (this.activeContext) {
      const benchmarker = this.contextBenchmarkers.pop()

      this.log(this.indent(`${this.gray(`${this.activeContext.name}:`)} ⏱️  ${benchmarker?.getDuration()} milliseconds`))
    }

    super.stopContext()
  }

  public startTest(test: Test): void {
    this.testBenchmarker.start()
    super.startTest(test)
  }

  public stopTest(error?: Error): void {
    super.stopTest(error)

    this.log(this.indent(`  ⏱️  ${this.testBenchmarker.getDuration()} milliseconds`, true))
  }
}
