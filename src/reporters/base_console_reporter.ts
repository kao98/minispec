import BaseReporter from './base_reporter.js'
import { Writable } from 'stream'

export default class BaseConsoleReporter extends BaseReporter {
  protected readonly outStream: Writable
  protected readonly errorStream: Writable

  constructor(
    outStream?: Writable,
    errorStream?: Writable
  ) {
    super()

    this.outStream = outStream || process.stdout
    this.errorStream = errorStream || process.stderr
  }

  protected log(message: string): void {
    this.outStream.write(`${message}\n`)
  }

  protected error(message: string): void {
    this.errorStream.write(`${message}\n`)
  }

  protected green(message: string): string {
    return `\x1b[32m${message}\x1b[0m`
  }

  protected red(message: string): string {
    return `\x1b[31m${message}\x1b[0m`
  }

  protected yellow(message: string): string {
    return `\x1b[33m${message}\x1b[0m`
  }

  protected gray(message: string): string {
    return `\x1b[90m${message}\x1b[0m`
  }
}
