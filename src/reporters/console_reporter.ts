import IReporter from './ireporter.js'
import BaseConsoleReporter from './base_console_reporter.js'
import SummaryReporter from './summary_reporter.js'
import Context from '../runnables/context.js'
import Test from '../runnables/test.js'
import { Writable } from 'stream'

export default class ConsoleReporter extends BaseConsoleReporter {
  private summaryReporter: IReporter

  constructor(
    summaryReporter?: IReporter,
    outStream?: Writable,
    errorStream?: Writable
  ) {
    super(outStream, errorStream)
    this.summaryReporter = summaryReporter || new SummaryReporter(this.outStream, this.errorStream)
  }

  public startContext(context: Context): void {
    if (this.activeContexts.length === 0) {
      this.log('')
    }

    super.startContext(context)

    if (context.isSkipped()) {
      this.log(
        this.indent(
          this.yellow(
            `${context.name} (SKIPPED)`
          )
        )
      )
    } else {
      this.log(this.indent(context.name))
    }
    this.summaryReporter.startContext(context)
  }

  public stopContext(): void {
    super.stopContext()
    this.summaryReporter.stopContext()
  }

  public startTest(test: Test): void {
    super.startTest(test)
    this.summaryReporter.startTest(test)
  }

  public stopTest(error?: Error): void {
    const activeTest = this.activeTest

    super.stopTest()

    const indentedTestName = this.indent(activeTest?.name || '', true)

    if (error) {
      if (error.message === 'skipped') {
        this.error(this.yellow(`${indentedTestName} (SKIPPED)`))
      } else {
        this.error(this.red(`${indentedTestName} (FAILED)`))
      }
    } else {
      this.log(this.green(indentedTestName))
    }

    this.summaryReporter.stopTest(error)
  }

  public summarizeExecution(): void {
    this.summaryReporter.summarizeExecution()
  }

  public startTimer(timeInMilliseconds?: number) {
    this.summaryReporter.startTimer(timeInMilliseconds)
  }

  public stopTimer(timeInMilliseconds?: number) {
    this.summaryReporter.stopTimer(timeInMilliseconds)
  }

  public addTimerInterval(timeInMilliseconds?: number, intervalName?: string) {
    this.summaryReporter.addTimerInterval(timeInMilliseconds, intervalName)
  }

  protected indent(message: string, isTest = false): string {
    const level = this.activeContexts.length - 1
    const extra = isTest ? 3 : 1
    const indentation = Array(level * 2 + extra).join(' ')

    return `${indentation}${message}`
  }
}
