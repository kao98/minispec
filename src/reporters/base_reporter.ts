import Context from '../runnables/context.js'
import Test from '../runnables/test.js'
import IReporter from './ireporter.js'

export default class BaseReporter implements IReporter {
  protected activeContexts: Context[] = []
  protected activeTest: Test | null = null

  protected timerStartedAt: number
  protected timerStopedAt: number
  protected timerIntervals: Map<string, number>[]

  constructor() {
    this.timerStartedAt = 0
    this.timerStopedAt = 0
    this.timerIntervals = []
  }

  public startContext(context: Context): void {
    if (this.activeTest) {
      throw new Error('Test in progress, stop it before starting a new context')
    }

    this.activeContexts.push(context)
  }

  public stopContext(): void {
    if (this.activeTest) {
      throw new Error('Test in progress, stop it before stopping active context')
    }

    if (this.activeContexts.length) {
      this.activeContexts.pop()
      return
    }

    throw new Error('No active context to stop')
  }

  public startTest(test: Test): void {
    if (this.activeTest) {
      throw new Error('Test in progress, stop it before starting a new one')
    }

    this.activeTest = test
  }

  public stopTest(): void {
    if(!this.activeTest) {
      throw new Error('No active test to stop')
    }

    this.activeTest = null
  }

  public summarizeExecution(): void {
    if (this.activeTest) {
      throw new Error('Test in progress, stop it before asking for execution summary')
    }

    if (this.activeContexts.length) {
      throw new Error('Context in progress, stop it before asking for execution summary')
    }
  }

  public startTimer(timeInMilliseconds?: number): void {
    this.timerStartedAt = BaseReporter.getActualTime(timeInMilliseconds)
  }

  public addTimerInterval(timeInMilliseconds?: number, intervalName?: string): void {
    const interval = new Map<string, number>()

    interval.set(
      intervalName || `interval #${this.timerIntervals.length}`,
      BaseReporter.getActualTime(timeInMilliseconds)
    )

    this.timerIntervals.push(interval)
  }

  public stopTimer(timeInMilliseconds?: number): void {
    this.timerStopedAt = BaseReporter.getActualTime(timeInMilliseconds)
  }

  protected static getActualTime(timeInMilliseconds?: number): number {
    if (timeInMilliseconds === undefined) {
      return Date.now()
    }

    return timeInMilliseconds
  }

  protected get activeContext(): Context | undefined {
    if (this.activeContexts.length) {
      return this.activeContexts[this.activeContexts.length - 1]
    }

    return undefined
  }
}
