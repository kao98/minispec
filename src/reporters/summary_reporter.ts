import BaseConsoleReporter from './base_console_reporter.js'

interface Failure {
  testName: string,
  testLocation?: string,
  error?: {
    message?: string,
    stack?: string,
    expected?: unknown,
    actual?: unknown,
    operator?: string
  },
  stack?: string
}

export default class SummaryReporter extends BaseConsoleReporter {
  private skipped: Failure[] = []
  private errors: Failure[] = []
  private numberOfExecutedTest = 0

  public stopTest(error?: Error): void {
    const activeTest = this.activeTest

    super.stopTest()

    this.numberOfExecutedTest++

    if (error) {
      const fullContextName = this.activeContexts.map((c) => c.name).join(', ')
      const fullTestName = `${fullContextName}, it ${activeTest?.name}`

      if (error.message === 'skipped') {
        this.skipped.push({ testName: fullTestName, testLocation: activeTest?.location })
      } else {
        this.errors.push({ testName: fullTestName, testLocation: activeTest?.location, error: error, stack: error.stack })
      }
    }

    this.activeTest = null
  }

  public summarizeExecution(): void {
    super.summarizeExecution()

    this.log('')

    if (this.skipped.length) {
      this.reportSkipped()
    }

    if (this.errors.length) {
      this.reportErrors()
    }

    if (this.timerStopedAt) {
      let intervals = ''

      if (this.timerIntervals.length) {
        let intervalStartingValue = this.timerStartedAt

        this.timerIntervals.forEach((interval) => {
          if (intervals.length) {
            intervals = `${intervals}, `
          }

          const intervalName = interval.keys().next().value
          const intervalValue = interval.values().next().value

          intervals = `${intervals}${intervalName} took ${intervalValue - intervalStartingValue} milliseconds`

          intervalStartingValue = intervalValue
        })

        intervals = ` (${intervals.trim()})`
      }

      this.log(`Finished in ${this.timerStopedAt - this.timerStartedAt} milliseconds${intervals}`)
    } else {
      this.log('Finished')
    }

    const numberOfExecutedTest = this.numberOfExecutedTest - this.skipped.length
    if (numberOfExecutedTest) {
      let skippedString = ''

      if (this.skipped.length) {
        skippedString = ` (${this.skipped.length} skipped)`
      }

      if (this.errors.length) {
        this.log(
          this.red(
            `${this.pluralize('test', numberOfExecutedTest)}${skippedString}, ${this.pluralize('failure', this.errors.length)}`
          )
        )

        this.reportFailuresLocations()
      } else {
        this.log(this.green(`${this.pluralize('test', numberOfExecutedTest)}${skippedString}, no failure 👏`))
      }
    } else {
      if (this.skipped.length) {
        this.log(this.yellow(`No test has been executed (${this.skipped.length} skipped) 🤔`))
      } else {
        this.log(this.yellow('No test has been executed 🤔'))
      }
    }
  }

  private reportSkipped(): void {
    this.error('Skipped:')

    this.skipped.forEach((skipped, index) => {
      const prefix = `  ${index + 1})`
      this.error('')
      this.error(`${prefix} ${this.yellow(skipped.testName)}`)
      this.error(
        this.pad(
          this.yellow(
            `# ${skipped.testLocation}`
          ),
          prefix.length + 1
        )
      )

      this.error('')
    })
  }

  private reportErrors(): void {
    this.error('Failures:')

    this.errors.forEach((error, index) => {
      const prefix = `  ${index + 1})`
      this.error('')
      this.error(`${prefix} ${error.testName}`)

      if (error.error?.message) {
        const message = error.error.message

        this.error(this.pad(this.red(message), prefix.length + 1))
      }

      if (error.error?.actual && error.error?.expected) {
        let expected = error.error.expected
        if (error.error.operator && error.error.operator.startsWith('not')) {
          expected = `not ${expected}`
        }

        this.error('')
        this.error(this.red(this.pad(`  expected: ${expected}`, prefix.length + 1)))
        this.error(this.red(this.pad(`       got: ${error.error.actual}`, prefix.length + 1)))
      }

      if (error.error?.operator) {
        this.error('')
        this.error(this.red(this.pad(`  (compared using ${error.error.operator})`, prefix.length + 1)))
      }

      if (error.stack) {
        this.error(
          this.pad(
            this.green(
              `# ${this.findSpec(error.stack)}`
            ),
            prefix.length + 1
          )
        )
      }

      this.error('')
    })
  }

  private reportFailuresLocations(): void {
    this.error('')
    this.error('Spec with failures:')
    this.error('')

    for (const error of this.errors) {
      const location = error.testLocation || (error.stack && this.findSpec(error.stack)) || undefined

      if (location === undefined) {
        return
      }

      this.error(this.red(location))
    }
  }

  private pad(message: string, length: number): string {
    const pad = Array(length + 1).join(' ')

    return message.split('\n').map((m) => m != '' ? `${pad}${m}` : m).join('\n')
  }

  private findSpec(stack: string): string {
    const regexp = /^at[\s]*.*$/

    try {
      const matches = stack
        .split('\n')
        .filter((m) => regexp.test(m.trim()))[0]
        .trim()
        .match(/^at\s*(.*)$/)

      if (matches && matches.length > 1) {
        return matches[1]
      }
    } catch {
      // We've not been able to properly find spec files in the stack trace
      // We just ignore the error
    }

    return ''
  }

  private pluralize(word: string, number: number): string {
    return `${number} ${word}${number > 1 ? 's' : ''}`
  }
}
