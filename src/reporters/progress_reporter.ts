import SummaryReporter from './summary_reporter.js'
import Context from '../runnables/context.js'

export default class ProgressReporter extends SummaryReporter {
  private initialNewlinePrinted = false

  public startContext(context: Context): void {
    if (!this.initialNewlinePrinted) {
      this.outStream.write('\n')
      this.initialNewlinePrinted = true
    }
    super.startContext(context)
  }

  public stopTest(error?: Error): void {
    super.stopTest(error)

    if (error) {
      if (error.message === 'skipped') {
        this.outStream.write(this.yellow('S'))
      } else {
        this.outStream.write(this.red('F'))
      }
    } else {
      this.outStream.write(this.green('.'))
    }
  }

  public summarizeExecution(): void {
    this.outStream.write('\n')
    super.summarizeExecution()
  }
}
