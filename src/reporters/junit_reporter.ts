import BaseReporter from './base_console_reporter.js'
import Benchmarker from '../services/benchmarker.js'
import Context from '../runnables/context.js'
import Test from '../runnables/test.js'
import { Writable } from 'stream'

export default class JunitReporter extends BaseReporter {
  private xmlHeaderHasBeenPrinted = false
  private lastReportedContext: Context | undefined = undefined

  constructor(
    protected readonly stream: Writable = process.stdout,
    protected readonly benchmarker: Benchmarker = new Benchmarker()
  ) {
    super()
  }

  public startContext(context: Context): void {
    if (this.xmlHeaderHasBeenPrinted === false) {
      this.stream.write(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n`)
      this.reportTestsuites()
      this.xmlHeaderHasBeenPrinted = true
    }

    if (this.activeContext && this.activeContext === this.lastReportedContext) {
      this.reportTestsuiteFinished()
      this.lastReportedContext = undefined
    }

    super.startContext(context)
  }

  public stopContext(): void {
    if (this.activeContext && this.activeContext === this.lastReportedContext) {
      this.reportTestsuiteFinished()
      this.lastReportedContext = undefined
    }

    super.stopContext()
  }

  public startTest(test: Test): void {
    this.benchmarker.start()

    if (this.activeContext !== this.lastReportedContext) {
      this.reportTestsuite()
      this.lastReportedContext = this.activeContext
    }

    super.startTest(test)
  }

  public stopTest(error?: Error): void {
    this.reportTestcase(this.benchmarker.getRawDuration() / 1000)

    if (error) {
      this.reportFailure(error)
    }

    this.reportTestcaseFinished()

    super.stopTest()
  }

  public summarizeExecution(): void {
    if (this.xmlHeaderHasBeenPrinted) {
      this.reportTestsuitesFinished()
    }

    super.summarizeExecution()
  }

  private reportTestsuites(): void {
    this.addToReport(0, 'testsuites')
  }

  private reportTestsuite(): void {
    const fullContextName = this.activeContexts.map((c) => c.name).join(', ')

    this.addToReport(
      2,
      'testsuite',
      new Map(Object.entries({
        "name": fullContextName,
        "tests": this.activeContext?.numberOfTests,
        "timestamp": Date.now()
      }))
    )
  }

  private reportTestcase(durationInSeconds: number): void {
    const fullContextName = this.activeContexts.map((c) => c.name).join(', ')

    const roundedDurationInSeconds = Math.round((durationInSeconds + Number.EPSILON) * 100000) / 100000

    this.addToReport(
      4,
      'testcase',
      new Map(Object.entries({
        "name": this.activeTest?.name,
        "classname": fullContextName,
        "time": roundedDurationInSeconds
      }))
    )
  }

  private reportFailure(error: Error): void {
    this.addToReport(
      6,
      'failure',
      new Map(Object.entries({
        "message": error.message,
        "type": error.name
      }))
    )
    this.addToReport(6, '/failure')
  }

  private reportTestcaseFinished(): void {
    this.addToReport(4, '/testcase')
  }

  private reportTestsuiteFinished(): void {
    this.addToReport(2, '/testsuite')
  }

  private reportTestsuitesFinished(): void {
    this.addToReport(0, '/testsuites')
  }

  private addToReport(indent: number, nodeName: string, attributes: Map<string, string | number | undefined> = new Map()): void {
    let serializedAttributes = ''

    attributes.forEach((attributeValue: string | number | undefined, attributeName: string) => {
      attributeValue = attributeValue
        ?.toString()
        .replace(/"/g, "&quot;")
        .replace(/</g, "&lt;")

      serializedAttributes += ` ${attributeName}=${JSON.stringify(attributeValue?.toString())}`
    })

    this.stream.write(Array(indent + 1).join(' '))
    this.stream.write(`<${nodeName}${serializedAttributes}>`)
    this.stream.write('\n')
  }
}
