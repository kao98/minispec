import Test from './test.js'
import Hook from './hook.js'
import Engine, { IEngine } from '../engine/engine.js'
import ActiveContext from '../engine/active_context.js'

interface IContextOptions {
  engine?: IEngine,
  forced?: boolean,
  focused?: boolean,
  skipped?: boolean,
}

export default class Context {
  private beforeAllHooks: Hook[] = []
  private afterAllHooks: Hook[] = []
  private beforeEachHooks: Hook[] = []
  private afterEachHooks: Hook[] = []
  private tests: Test[] = []
  private focusedTests: Test[] = []
  private readonly engine: IEngine
  private readonly activeContext: ActiveContext
  private discovered = false
  protected readonly focused: boolean
  protected forced = false
  protected skipped = false
  public contexts: Context[] = []

  constructor(
    readonly name: string,
    protected readonly code: () => Promise<void>,
    options?: IContextOptions
  ) {
    this.engine = options?.engine || Engine.getInstance()
    this.activeContext = this.engine.activeContext
    this.focused = options?.focused || false
    this.forced = options?.forced || false
    this.skipped = options?.skipped || false
  }

  public async discover(): Promise<void> {
    if (this.discovered) {
      return
    }

    this.activeContext.set(this)

    await this.code()

    for (const context of this.contexts) {
      await context.discover()
    }

    this.discovered = true
  }

  public async execute(focusedOnly: boolean, beforeEachHooks: Hook[] = [], afterEachHooks: Hook[] = []): Promise<void> {
    this.activeContext.set(this)

    this.engine.reportContextStarted(this)

    for (const hook of this.beforeAllHooks) {
      await hook.execute()
    }

    const tests = this.findTestsToExecute(focusedOnly)

    for (const test of tests) {

      for (const hook of beforeEachHooks) {
        await hook.execute()
      }

      for (const hook of this.beforeEachHooks) {
        await hook.execute()
      }

      await test.execute()

      for (const hook of this.afterEachHooks) {
        await hook.execute()
      }

      for (const hook of afterEachHooks) {
        await hook.execute()
      }
    }

    const contexts = this.findContextsToExecute(focusedOnly)

    for (const context of contexts) {
      await context.execute(
        focusedOnly,
        [...beforeEachHooks, ...this.beforeEachHooks],
        [...afterEachHooks, ...this.afterEachHooks]
      )
    }

    for (const hook of this.afterAllHooks) {
      await hook.execute()
    }

    this.engine.reportContextFinished()
  }

  public addTest(test: Test, focused = false): void {
    if (this.skipped) {
      test.skip()
    }

    this.tests.push(test)
    if (focused) {
      this.focusedTests.push(test)
    }
  }

  public addBeforeAllHook(hook: Hook): void {
    this.beforeAllHooks.push(hook)
  }

  public addBeforeEachHook(hook: Hook): void {
    this.beforeEachHooks.push(hook)
  }

  public addAfterEachHook(hook: Hook): void {
    this.afterEachHooks.push(hook)
  }

  public addAfterAllHook(hook: Hook): void {
    this.afterAllHooks.push(hook)
  }

  public addChildContext(context: Context): void {
    if (this.skipped) {
      context.skip()
    }
    this.contexts.push(context)
  }

  public isSkipped(): boolean {
    return this.skipped
  }

  public skip(): void {
    this.skipped = true
  }

  public get numberOfTests(): number {
    return this.tests.length
  }

  private findTestsToExecute(focusedOnly: boolean): Test[] {
    if (focusedOnly) {
      if (this.focusedTests.length) {
        return this.focusedTests
      }

      return this.focused || this.forced ? this.tests : []
    }

    return this.tests
  }

  private findContextsToExecute(focusedOnly: boolean): Context[] {
    if (focusedOnly) {
      const focusedContext = this.contexts.filter((context) => context.focused)

      if (focusedContext.length) {
        return focusedContext
      }

      if (this.focused || this.forced) {
        return this.contexts.map((context) => {
          context.forced = true

          return context
        })
      }
    }

    return this.contexts
  }
}
