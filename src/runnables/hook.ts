export default class Hook {
  constructor(
    readonly code: () => Promise<void>
  ) {}

  public async execute(): Promise<void> {
    await this.code()
  }
}
