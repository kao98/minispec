import Engine, { IEngine } from '../engine/engine.js'

interface ITestOptions {
  engine?: IEngine,
  skipped?: boolean,
  location?: string,
}

export default class Test {
  private readonly engine: IEngine
  private skipped: boolean
  public readonly location: string | undefined

  constructor(
    readonly name: string,
    private readonly code: () => Promise<void>,
    options?: ITestOptions
  ) {
    this.engine = options?.engine || Engine.getInstance()
    this.skipped = options?.skipped || false
    this.location = options?.location
  }

  public async execute(): Promise<void> {
    this.engine.reportTestStarted(this)
    try {
      if (this.skipped) {
        throw new Error('skipped')
      }

      await this.code()

      this.engine.reportTestFinished()
    } catch (error) {
      this.engine.reportTestFailed(error)
    }
  }

  public skip(): void {
    this.skipped = true
  }
}
