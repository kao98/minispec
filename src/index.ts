import describe, { fdescribe, xdescribe, sdescribe } from './api/describe.js'
import context, { fcontext, xcontext, scontext } from './api/context.js'
import it, { fit, xit, sit } from './api/it.js'
import beforeAll from './api/before_all.js'
import beforeEach from './api/before_each.js'
import afterEach from './api/after_each.js'
import afterAll from './api/after_all.js'

import Engine, { IEngine } from './engine/engine.js'

const MiniSpec = Engine

export {
  describe,
  fdescribe,
  xdescribe,
  sdescribe,
  context,
  fcontext,
  xcontext,
  scontext,
  it,
  fit,
  xit,
  sit,
  beforeAll,
  beforeEach,
  afterEach,
  afterAll,
  IEngine
}

export default MiniSpec
