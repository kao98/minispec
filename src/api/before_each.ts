import Engine from '../engine/engine.js'

/**
 * Execute arbitrary code before each example of the current group (describe or context) is run.
 * 
 * @remarks
 * The hook is executed one time for each example in the current group,
 * including children groups if any.
 * 
 * @param code - async function to execute before each example is run
 * 
 * @example
 * ```
 * describe('Hooks', async () => {
 *   beforeEach(async ()   => {
 *     // Will be executed a single time before each example of the `Hooks` group is run,
 *     // including the examples of `child context`
 *   })
 * 
 *   context('child context', async () => {
 *     // snip...
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/04-Before-and-After-hooks | `Hooks` documentation for more details}
 */
export default function beforeEach(code: () => Promise<void>) {
  Engine.getInstance().runtime.addBeforeEachHook(code)
}
