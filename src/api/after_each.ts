import Engine from '../engine/engine.js'

/**
 * Execute arbitrary code after each example in the current group (context or
 * describe) has been run.
 * 
 * @remarks
 * The hook is executed one time for each example in the current group,
 * including children groups if any.
 * 
 * @param code - async function to execute after each example has been run
 * 
 * @example
 * ```
 * describe('Hooks', async () => {
 *   afterEach(async ()   => {
 *     // Will be executed after each examples in the `Hooks` group,
 *     // including the examples in the `child context`.
 *   })
 * 
 *   context('child context', async () => {
 *     // snip...
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/04-Before-and-After-hooks | `Hooks` documentation for more details}
 */
export default function afterEach(code: () => Promise<void>) {
  Engine.getInstance().runtime.addAfterEachHook(code)
}
