import Engine from '../engine/engine.js'

/**
 * `it` declares an example, a test. This is the method which will run a test.
 * 
 * @remarks
 * An example - or test - must be part of an example group. The following example
 * shows the whole code required to run a test.
 * 
 * @param name - name of your example
 * @param code - async function which contains the code of your example
 * 
 * @example
 * ```
 * import { describe, it } from 'minispec'
 * 
 * describe('An example group', async () => {
 *   it('contains an example', async () => {
 *     // Here's some code which will implement your test
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs#examples-tests | `Writing Specs / Examples (tests)` documentation for more details}
 * @see {@link it.focus}
 * @see {@link it.skip}
 * @see {@link it.ignores}
 */
function it(name: string, code: () => Promise<void>) {
  Engine
    .getInstance()
    .runtime
    .addTest(
      name,
      code,
      {
        location: findLocation()
      }
    )
}

/**
 * Gives the focus to an example. As soon as examples or groups
 * have the focus, they will be the only ones to be executed.
 * 
 * @param name - name of your example
 * @param code - async function which contains the code of your example
 * 
 * @example
 * ```
 * import { describe, it, fit } from 'minispec'
 * 
 * describe('An example group', async () => {
 *   fit('contains an example', async () => {
 *     // this one will have the focus
 *   })
 *   it.focus('contains an example', async () => {
 *     // this one will also have the focus
 *   })
 *   it.f('contains an example', async () => {
 *     // this one too will have the focus
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link it}
 * @see {@link it.skip}
 * @see {@link it.ignores}
 */
export function fit(name: string, code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to focus on some tests on a CI')
  }

  Engine
    .getInstance()
    .runtime
    .addTest(
      name,
      code,
      {
        focused: true,
        location: findLocation()
      }
    )
}

/**
 * Ignores the example.
 * 
 * Such example are totally ignored during the execution. They won't be reported at all.
 * 
 * @remarks
 * If it is detected that the execution is part of a CI, an error is thrown. This
 * behavior aims to prevent forgetting tests. If you need to exclude a test from
 * your CI, it is recommanded to use {@link it.skip} instead.
 * 
 * @param name - name of your example
 * @param code - async function which contains the code of your example
 * 
 * @example
 * ```
 * import { describe, it, xit } from 'minispec'
 * 
 * describe('An example group', async () => {
 *   xit('contains an example', async () => {
 *     // this one will be ignored
 *   })
 *   it.ignores('contains an example', async () => {
 *     // this one will also be ignored
 *   })
 *   it.x('contains an example', async () => {
 *     // this one too will be ignored
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link it}
 * @see {@link it.skip}
 * @see {@link it.focus}
 */
export function xit(_name: string, _code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to ignore tests on a CI')
  }
}

/**
 * Skip the example.
 * 
 * Such example are reported as skipped during the execution.
 * 
 * @param name - name of your example
 * @param code - async function which contains the code of your example
 * 
 * @example
 * ```
 * import { describe, it, sit } from 'minispec'
 * 
 * describe('An example group', async () => {
 *   sit('contains an example', async () => {
 *     // this one will be skipped
 *   })
 *   it.skip('contains an example', async () => {
 *     // this one will also be skipped
 *   })
 *   it.s('contains an example', async () => {
 *     // this one too will be skipped
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link it}
 * @see {@link it.ignores}
 * @see {@link it.focus}
 */
export function sit(name: string, code: () => Promise<void>) {
  Engine
    .getInstance()
    .runtime
    .addTest(
      name,
      code,
      {
        skipped: true,
        location: findLocation()
      }
    )
}

it.f = fit
it.focus = fit

it.x = xit
it.ignores = xit

it.s = sit
it.skip = sit

export default it

function findLocation(): string | undefined {
  const regexp = /^at[\s]*.*$/
  const stack = new Error().stack || ''

  try {
    const matches = stack
      .split('\n')
      .filter((m) => regexp.test(m.trim()))[2]
      .trim()
      .match(/^at\s*(.*)$/)

    if (matches && matches.length > 0) {
      return matches[0]
    }
  } catch {
    // We've not been able to properly find the location in the stack trace
    // we just ignore it
  }

  return undefined
}
