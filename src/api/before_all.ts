import Engine from '../engine/engine.js'

/**
 * Execute arbitrary code one time before the examples of the current group are run.
 * 
 * @param code - async function to execute before examples are run
 * 
 * @example
 * ```
 * describe('Hooks', async () => {
 *   beforeAll(async ()   => {
 *     // Will be executed a single time before the examples of the `Hooks` group are run,
 *     // including the examples of `child context`
 *   })
 * 
 *   context('child context', async () => {
 *     // snip...
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/04-Before-and-After-hooks | `Hooks` documentation for more details}
 */
export default function beforeAll(code: () => Promise<void>) {
  Engine.getInstance().runtime.addBeforeAllHook(code)
}
