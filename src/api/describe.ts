import Engine from '../engine/engine.js'

/**
 * Describe sections are example groups. It helps organizing tests, grouping them
 * together based on the test subject for example.
 * 
 * @remarks
 * Describe is one of the two existing example groups. The other one is
 * {@link context}.
 * 
 * @param name - name of your group
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, context } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   context('.someMethod()', async () => {
 *     // Here's some code including children groups, and examples
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs#example-groups | `Writing Specs / Example groups` documentation for more details}
 * @see {@link describe.focus}
 * @see {@link describe.skip}
 * @see {@link describe.ignores}
 * @see {@link context}
 */
function describe(name: string, code: () => Promise<void>) {
  Engine.getInstance().runtime.addContext(name, code)
}

/**
 * Gives the focus to an example group. As soon as examples or groups
 * have the focus, they will be the only ones to be executed.
 * 
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, fdescribe } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   fdescribe('.someMethod()', async () => {
 *     // this one will have the focus
 *   })
 *   describe.focus('.someMethod()', async () => {
 *     // this one will also have the focus
 *   })
 *   describe.f('.someMethod()', async () => {
 *     // this one too will have the focus
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link describe}
 * @see {@link describe.skip}
 * @see {@link describe.ignores}
 */
export function fdescribe(name: string, code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to focus on some "describe" blocks on a CI')
  }

  Engine.getInstance().runtime.addContext(name, code, { focused: true })
}

/**
 * Skip the example group.
 * 
 * Such groups - including sub-groups and tests - are reported as skipped during
 * the execution.
 *  
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, sdescribe } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   sdescribe('.someMethod()', async () => {
 *     // this one will be skipped
 *   })
 *   describe.s('.someMethod()', async () => {
 *     // this one will also be skipped
 *   })
 *   describe.skip('.someMethod()', async () => {
 *     // this one too will be skipped
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link context.focus}
 * @see {@link context.ignores}
 * @see {@link context}
 */
export function sdescribe(name: string, code: () => Promise<void>) {
  Engine.getInstance().runtime.addContext(name, code, { skipped :true })
}

/**
 * Ignores the example group.
 * 
 * Such groups are totally ignored during the execution. They won't be reported at all.
 * 
 * @remarks
 * If it is detected that the execution is part of a CI, an error is thrown. This
 * behavior aims to prevent forgetting tests. If you need to exclude a test from
 * your CI, it is recommanded to use {@link describe.skip} instead.
 * 
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, xdescribe } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   xdescribe('.someMethod()', async () => {
 *     // this one will be ignored
 *   })
 *   describe.ignores('.someMethod()', async () => {
 *     // this one will also be ignored
 *   })
 *   describe.x('.someMethod()', async () => {
 *     // this one too will be ignored
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link describe.focus}
 * @see {@link describe.skip}
 * @see {@link describe}
 */
export function xdescribe(_name: string, _code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to ignore "describe" blocks on a CI')
  }
}

describe.f = fdescribe
describe.focus = fdescribe

describe.x = xdescribe
describe.ignores = xdescribe

describe.s = sdescribe
describe.skip = sdescribe

export default describe
