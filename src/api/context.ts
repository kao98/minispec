import Engine from '../engine/engine.js'

/**
 * Context are example groups. It helps organizing tests, grouping them together
 * based on some shared requirements for example.
 * 
 * @remarks
 * Context is one of the two existing example groups. The other one is
 * {@link describe}.
 * 
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, context } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   context('.someMethod()', async () => {
 *     // Here's some code including children groups, and examples
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs#example-groups | `Writing Specs / Example groups` documentation for more details}
 * @see {@link context.focus}
 * @see {@link context.skip}
 * @see {@link context.ignores}
 * @see {@link describe}
 */
function context(name: string, code: () => Promise<void>) {
  Engine.getInstance().runtime.addContext(name, code)
}

/**
 * Gives the focus to an example group. As soon as examples or groups
 * have the focus, they will be the only ones to be executed.
 * 
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, context, fcontext } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   fcontext('.someMethod()', async () => {
 *     // this one will have the focus
 *   })
 *   context.focus('.someMethod()', async () => {
 *     // this one will also have the focus
 *   })
 *   context.f('.someMethod()', async () => {
 *     // this one too will have the focus
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link context}
 * @see {@link context.skip}
 * @see {@link context.ignores}
 */
export function fcontext(name: string, code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to focus on some contexts on a CI')
  }

  Engine.getInstance().runtime.addContext(name, code, { focused: true })
}

/**
 * Ignores the example group.
 * 
 * Such groups are totally ignored during the execution. They won't be reported at all.
 * 
 * @remarks
 * If it is detected that the execution is part of a CI, an error is thrown. This
 * behavior aims to prevent forgetting tests. If you need to exclude a test from
 * your CI, it is recommanded to use {@link context.skip} instead.
 * 
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, context, xcontext } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   xcontext('.someMethod()', async () => {
 *     // this one will be ignored
 *   })
 *   context.ignores('.someMethod()', async () => {
 *     // this one will also be ignored
 *   })
 *   context.x('.someMethod()', async () => {
 *     // this one too will be ignored
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link context.focus}
 * @see {@link context.skip}
 * @see {@link context}
 */
export function xcontext(_name: string, _code: () => Promise<void>) {
  if (process.env['CI']) {
    throw new Error('It is not allowed to ignore contexts on a CI')
  }
}

/**
 * Skip the example group.
 * 
 * Such groups - including sub-groups and tests - are reported as skipped during
 * the execution.
 *  
 * @param name - name of your context
 * @param code - async function which contains the code of your group, including hooks, children groups and examples
 * 
 * @example
 * ```
 * import { describe, context, scontext } from 'minispec'
 * 
 * describe('SomeClass', async () => {
 *   scontext('.someMethod()', async () => {
 *     // this one will be skipped
 *   })
 *   context.s('.someMethod()', async () => {
 *     // this one will also be skipped
 *   })
 *   context.skip('.someMethod()', async () => {
 *     // this one too will be skipped
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/03-Writing-Specs##skipping-focusing-ignoring | `Writing Specs / Skipping, Focusing, Ignoring` documentation for more details}
 * @see {@link context.focus}
 * @see {@link context.ignores}
 * @see {@link context}
 */
export function scontext(name: string, code: () => Promise<void>) {
  Engine.getInstance().runtime.addContext(name, code, { skipped: true })
}

context.f = fcontext
context.focus = fcontext

context.x = xcontext
context.ignores = xcontext

context.s = scontext
context.skip = scontext

export default context
