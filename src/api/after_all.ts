import Engine from '../engine/engine.js'

/**
 * Execute arbitrary code after all examples of the current group (context or
 * describe) have been run.
 * 
 * @remarks
 * The hook is executed one time for the whole group regarding there are
 * children groups or not.
 * 
 * @param code - async function to execute after all examples have been run
 * 
 * @example
 * ```
 * describe('Hooks', async () => {
 *   afterAll(async ()   => {
 *     // Will be executed a single time after all the examples in the `Hooks` group have been run,
 *     // including the examples in the `child context`
 *   })
 * 
 *   context('child context', async () => {
 *     // snip...
 *   })
 * })
 * ```
 * 
 * @public
 * 
 * @see {@link https://gitlab.com/kao98/minispec/-/wikis/Documentation/04-Before-and-After-hooks | `Hooks` documentation for more details}
 */
export default function afterAll(code: () => Promise<void>) {
  Engine.getInstance().runtime.addAfterAllHook(code)
}
