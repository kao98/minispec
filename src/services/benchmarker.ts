import { performance } from 'perf_hooks'

export default class Benchmarker {
  private startedAt = 0

  public start(timeInMilliseconds?: number): void {
    this.startedAt = Benchmarker.getActualTime(timeInMilliseconds)
  }

  public getRawDuration(timeInMilliseconds?: number): number {
    return (Benchmarker.getActualTime(timeInMilliseconds)) - this.startedAt
  }

  public getDuration(): number {
    const duration = this.getRawDuration() + Number.EPSILON
    return Math.round(duration * 1000) / 1000
  }

  protected static getActualTime(timeInMilliseconds?: number): number {
    if (timeInMilliseconds === undefined) {
      return performance.now()
    }

    return timeInMilliseconds
  }
}
