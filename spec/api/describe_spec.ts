import assert from 'assert/strict'
import { describe, context, it, beforeEach, afterEach } from '../../src/index.js'

describe('describe', async () => {
  describe('#ignore', async () => {
    context('when on CI', async () => {
      let originalValueOfEnvCI: string | undefined

      beforeEach(async () => {
        originalValueOfEnvCI = process.env['CI']

        process.env['CI'] = '1'
      })

      afterEach(async () => {
        process.env['CI'] = originalValueOfEnvCI
      })

      it('raises an error with message "not allowed"', async () => {
        try {
          describe.ignores('some describe', async () => {
            assert.fail('should not have executed that test')
          })

          assert.fail('should have raised an error before')
        } catch (error) {
          assert.equal(
            error.message,
            'It is not allowed to ignore "describe" blocks on a CI'
          )
        }
      })
    })
  })

  describe('#focus', async () => {
    context('when on CI', async () => {
      let originalValueOfEnvCI: string | undefined

      beforeEach(async () => {
        originalValueOfEnvCI = process.env['CI']

        process.env['CI'] = '1'
      })

      afterEach(async () => {
        process.env['CI'] = originalValueOfEnvCI
      })

      it('raises an error with message "not allowed"', async () => {
        try {
          describe.focus('some context', async () => {
            assert.fail('should not have executed that test')
          })

          assert.fail('should have raised an error before')
        } catch (error) {
          assert.equal(
            error.message,
            'It is not allowed to focus on some "describe" blocks on a CI'
          )
        }
      })
    })
  })
})
