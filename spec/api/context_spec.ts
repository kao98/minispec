import assert from 'assert/strict'
import { describe, context, it, beforeEach, afterEach } from '../../src/index.js'

describe('context', async () => {
  describe('#ignore', async () => {
    context('when on CI', async () => {
      let originalValueOfEnvCI: string | undefined

      beforeEach(async () => {
        originalValueOfEnvCI = process.env['CI']

        process.env['CI'] = '1'
      })

      afterEach(async () => {
        process.env['CI'] = originalValueOfEnvCI
      })

      it('raises an error with message "not allowed"', async () => {
        try {
          context.ignores('some context', async () => {
            assert.fail('should not have executed that test')
          })

          assert.fail('should have raised an error before')
        } catch (error) {
          assert.equal(
            error.message,
            'It is not allowed to ignore contexts on a CI'
          )
        }
      })
    })
  })

  describe('#focus', async () => {
    context('when on CI', async () => {
      let originalValueOfEnvCI: string | undefined

      beforeEach(async () => {
        originalValueOfEnvCI = process.env['CI']

        process.env['CI'] = '1'
      })

      afterEach(async () => {
        process.env['CI'] = originalValueOfEnvCI
      })

      it('raises an error with message "not allowed"', async () => {
        try {
          context.focus('some context', async () => {
            assert.fail('should not have execute that test')
          })

          assert.fail('should have raised an error before')
        } catch (error) {
          assert.equal(
            error.message,
            'It is not allowed to focus on some contexts on a CI'
          )
        }
      })
    })
  })
})
