import { Writable } from 'stream'

export default class TestStream extends Writable {
  private logged: string[] = []

  public _write(chunk: unknown, _encoding: string, next: (error?: Error) => void): void {
    this.logged.push(`${chunk}`)
    next()
  }

  public reset(): void {
    this.logged = []
  }

  public get logCalls(): string[] {
    return this.logged
  }

  public get loggedString(): string {
    return this.logCalls.join('')
  }
}
