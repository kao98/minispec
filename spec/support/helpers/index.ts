import Benchmarker from "../../../src/services/benchmarker.js"
import assert from "assert/strict"

export const emptyFunction = async () => { /* no-op */ }

export const assertStringIncludes = (haystack: string, needle: string): void => {
  assert.ok(
    haystack.includes(needle),
    `Expected string\x1b[0m\n\n"""\n${haystack}\n"""\n\n\x1b[31mto include\x1b[0m\n\n"""\n${needle}\n"""\n`
  )
}

export const assertStringDoesNotIncludes = (haystack: string, needle: string): void => {
  assert.equal(
    haystack.includes(needle),
    false,
    `Expected string\x1b[0m\n\n"""\n${haystack}\n"""\n\n\x1b[31mnot to include\x1b[0m\n\n"""\n${needle}\n"""\n`
  )
}

export class BenchmarkerStub extends Benchmarker {
  private increment = 10

  public setIncrement(newIncrement: number): void {
    this.increment = newIncrement
  }

  public start(): void {
    super.start(0)
  }

  public getRawDuration(): number {
    return super.getRawDuration(this.increment)
  }
}
