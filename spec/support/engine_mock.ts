import ActiveContext from '../../src/engine/active_context.js'
import { IEngine } from '../../src/engine/engine.js'
import { emptyFunction } from '../support/helpers/index.js'

export default class EngineMock implements IEngine {
  activeContext: ActiveContext

  constructor() {
    this.activeContext = new ActiveContext(this)
  }

  discover = async () => this
  execute = emptyFunction
  addReporter = () => this
  addReporters = () => this
  setReporter = () => this
  setReporters = () => this
  reportContextStarted = emptyFunction
  reportContextFinished = emptyFunction
  reportTestStarted = emptyFunction
  reportTestFinished = emptyFunction
  reportTestFailed = emptyFunction
}
