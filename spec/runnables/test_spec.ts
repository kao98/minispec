import assert from 'assert/strict'
import {
  describe,
  context,
  it,
  beforeEach,
} from '../../src/index.js'
import Test from '../../src/runnables/test.js'
import EngineMock from '../support/engine_mock.js'

class EngineSpy extends EngineMock {
  public errorReported: Error

  reportTestFailed = async (error?: Error) => {
    if (!error) {
      return
    }

    this.errorReported = error
  }
}

describe('Test', async () => {
  describe('#execute', async () => {
    let engine: EngineSpy

    beforeEach(async () => {
      engine = new EngineSpy()
    })

    context('when test is skipped', async () => {
      it('throws a "skipped" error', async () => {
        const test = new Test(
          'sut',
          async () => { /* no-op */ },
          {
            engine,
            skipped: true
          }
        )

        await test.execute()

        assert.deepStrictEqual(
          engine.errorReported?.message,
          'skipped'
        )
      })
    })

    context('when test is not skipped', async () => {
      it('executes the code of the test', async () => {
        let executed = false

        const test = new Test(
          'sut',
          async () => { executed = true },
          {
            engine,
            skipped: false
          }
        )

        await test.execute()

        assert.ok(executed)
      })
    })
  })
})
