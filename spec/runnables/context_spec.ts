import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  context,
  it,
} from '../../src/index.js'
import Context from '../../src/runnables/context.js'
import Test from '../../src/runnables/test.js'
import EngineMock from '../support/engine_mock.js'

class MockedContext extends Context {
  public async execute(): Promise<void> {
    await this.code()
  }

  public isForced(): boolean {
    return this.forced
  }

  public resetForced(): void {
    this.forced = false
  }

  public isSkipped(): boolean {
    return this.skipped
  }
}

describe('Context', async () => {
  describe('#execute', async () => {
    const engine = new EngineMock()

    let test1HasBeenExecuted = false
    let test2HasBeenExecuted = false
    let focusedTestHasBeenExecuted = false

    let context1HasBeenExecuted = false
    let context2HasBeenExecuted = false
    let focusedContextHasBeenExecuted = false

    const focusedTest = new Test(
      'Test 1',
      async () => { focusedTestHasBeenExecuted = true },
      { engine }
    )

    const test1 = new Test(
      'Test 1',
      async () => { test1HasBeenExecuted = true },
      { engine }
    )

    const test2 = new Test(
      'Test 2',
      async () => { test2HasBeenExecuted = true },
      { engine }
    )

    const context1 = new MockedContext(
      'Child 1',
      async () => { context1HasBeenExecuted = true },
      { engine }
    )

    const context2 = new MockedContext(
      'Child 2',
      async () => { context2HasBeenExecuted = true },
      { engine }
    )

    const focusedContext = new MockedContext(
      'focused child',
      async () => { focusedContextHasBeenExecuted = true },
      { engine, focused: true }
    )

    let sut: Context

    beforeEach(async () => {
      test1HasBeenExecuted = false
      test2HasBeenExecuted = false
      focusedTestHasBeenExecuted = false

      context1HasBeenExecuted = false
      context2HasBeenExecuted = false
      focusedContextHasBeenExecuted = false

      context1.resetForced()
      context2.resetForced()
      focusedContext.resetForced()

      sut = new Context('Context', async () => { /* no-op */ }, { engine })

      sut.addTest(test1)
      sut.addTest(test2)
      sut.addTest(focusedTest, true)

      sut.addChildContext(context1)
      sut.addChildContext(context2)
      sut.addChildContext(focusedContext)
    })

    context('when not requesting to execute focused tests and contexts', async () => {
      it('executes all tests and contexts', async () => {
        await sut.execute(false)

        assert.ok(test1HasBeenExecuted)
        assert.ok(test2HasBeenExecuted)
        assert.ok(focusedTestHasBeenExecuted)

        assert.ok(context1HasBeenExecuted)
        assert.ok(context2HasBeenExecuted)
        assert.ok(focusedContextHasBeenExecuted)
      })
    })

    context('when requesting to execute focused tests and contexts', async () => {
      it('executes only the focused tests and contexts', async () => {
        await sut.execute(true)

        assert.strictEqual(false, test1HasBeenExecuted)
        assert.strictEqual(false, test2HasBeenExecuted)
        assert.ok(focusedTestHasBeenExecuted)

        assert.strictEqual(false, context1HasBeenExecuted)
        assert.strictEqual(false, context2HasBeenExecuted)
        assert.ok(focusedContextHasBeenExecuted)
      })

      context('when context is focused', async () => {
        beforeEach(async () => {
          sut = new Context('Context', async () => { /* no-op */ }, { engine, focused: true })
        })

        context('without focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
          })

          it('executes all test', async () => {
            await sut.execute(true)

            assert.ok(test1HasBeenExecuted)
            assert.ok(test2HasBeenExecuted)
          })
        })

        context('with focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
            sut.addTest(focusedTest, true)
          })

          it('executes only focused test', async () => {
            await sut.execute(true)

            assert.strictEqual(false, test1HasBeenExecuted)
            assert.strictEqual(false, test2HasBeenExecuted)
            assert.ok(focusedTestHasBeenExecuted)
          })
        })

        context('without focused chidren', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
          })

          it('executes all contexts', async () => {
            await sut.execute(true)

            assert.ok(context1HasBeenExecuted)
            assert.ok(context2HasBeenExecuted)
          })

          it('forces execution of the chidren', async () => {
            await sut.execute(true)

            assert.ok(context1.isForced())
            assert.ok(context2.isForced())
          })
        })

        context('with focused children', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
            sut.addChildContext(focusedContext)
          })

          it('executes only focused children', async () => {
            await sut.execute(true)

            assert.strictEqual(false, context1HasBeenExecuted)
            assert.strictEqual(false, context2HasBeenExecuted)
            assert.ok(focusedContextHasBeenExecuted)
          })
        })
      })

      context('when context is forced', async () => {
        beforeEach(async () => {
          sut = new Context('Context', async () => { /* no-op */ }, { engine, forced: true })
        })

        context('without focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
          })

          it('executes all tests', async () => {
            await sut.execute(true)

            assert.ok(test1HasBeenExecuted)
            assert.ok(test2HasBeenExecuted)
          })
        })

        context('with focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
            sut.addTest(focusedTest, true)
          })

          it('executes only focused test', async () => {
            await sut.execute(true)

            assert.strictEqual(false, test1HasBeenExecuted)
            assert.strictEqual(false, test2HasBeenExecuted)
            assert.ok(focusedTestHasBeenExecuted)
          })
        })

        context('without focused chidren', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
          })

          it('executes all contexts', async () => {
            await sut.execute(true)

            assert.ok(context1HasBeenExecuted)
            assert.ok(context2HasBeenExecuted)
          })

          it('forces execution of the children', async () => {
            await sut.execute(true)

            assert.ok(context1.isForced())
            assert.ok(context2.isForced())
          })
        })

        context('with focused children', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
            sut.addChildContext(focusedContext)
          })

          it('executes only focused children', async () => {
            await sut.execute(true)

            assert.strictEqual(false, context1HasBeenExecuted)
            assert.strictEqual(false, context2HasBeenExecuted)
            assert.ok(focusedContextHasBeenExecuted)
          })
        })
      })

      context('when context is not focused', async () => {
        beforeEach(async () => {
          sut = new Context('Context', async () => { /* no-op */ }, { engine })
        })

        context('without focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
          })

          it('does not execute tests', async () => {
            sut.execute(true)

            assert.strictEqual(false, test1HasBeenExecuted)
            assert.strictEqual(false, test2HasBeenExecuted)
          })
        })

        context('with focused tests', async () => {
          beforeEach(async () => {
            sut.addTest(test1, false)
            sut.addTest(test2, false)
            sut.addTest(focusedTest, true)
          })

          it('executes only focused test', async () => {
            sut.execute(true)

            assert.strictEqual(false, test1HasBeenExecuted)
            assert.strictEqual(false, test2HasBeenExecuted)
            assert.ok(focusedTestHasBeenExecuted)
          })
        })

        context('without focused chidren', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
          })

          it('executes all contexts', async () => {
            await sut.execute(true)

            assert.ok(context1HasBeenExecuted)
            assert.ok(context2HasBeenExecuted)
          })

          it('does not force execution of children', async () => {
            await sut.execute(true)

            assert.strictEqual(false, context1.isForced())
            assert.strictEqual(false, context2.isForced())
          })
        })

        context('with focused children', async () => {
          beforeEach(async () => {
            sut.addChildContext(context1)
            sut.addChildContext(context2)
            sut.addChildContext(focusedContext)
          })

          it('executes only focused children', async () => {
            await sut.execute(true)

            assert.strictEqual(false, context1HasBeenExecuted)
            assert.strictEqual(false, context2HasBeenExecuted)
            assert.ok(focusedContextHasBeenExecuted)
          })
        })
      })
    })

    context('when context is skipped', async () => {
      beforeEach(async () => {
        sut = new Context('Context', async () => { /* no-op */ }, { engine, skipped: true })
      })

      it('skips tests', async () => {
        sut.addTest(test1, false)

        await sut.execute(false)

        assert.strictEqual(test1HasBeenExecuted, false)
      })

      it('skips children contexts', async () => {
        sut.addChildContext(context1)

        assert.ok(context1.isSkipped())
      })
    })
  })
})
