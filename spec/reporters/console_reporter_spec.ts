import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  context,
  it,
} from '../../src/index.js'
import ConsoleReporter from '../../src/reporters/console_reporter.js'
import Context from '../../src/runnables/context.js'
import Test from '../../src/runnables/test.js'
import TestStream from '../support/test_stream.js'
import { assertStringIncludes, emptyFunction } from '../support/helpers/index.js'
import { reindentTag as reindent } from 'reindent-template-literals'
import BaseReporter from '../../src/reporters/base_reporter.js'

class SummaryReporter extends BaseReporter {
  startContextCalled = false
  stopContextCalled = false
  startTestCalled = false
  stopTestCalled = false
  summarizeExecutionCalled = false

  startContext = (_: Context) => { this.startContextCalled = true }
  stopContext = () => { this.stopContextCalled = true }
  startTest = (_: Test) => { this.startTestCalled = true }
  stopTest = () => { this.stopTestCalled = true }
  summarizeExecution = () => { this.summarizeExecutionCalled = true }

  reset = () => {
    this.startContextCalled = false
    this.stopContextCalled = false
    this.startTestCalled = false
    this.stopTestCalled = false
    this.summarizeExecutionCalled = false
  }
}

describe('ConsoleReporter', async () => {
  const outStream = new TestStream()
  const errorStream = new TestStream()

  let consoleReporter: ConsoleReporter

  beforeEach(async () => {
    outStream.reset()
    errorStream.reset()

    consoleReporter = new ConsoleReporter(undefined, outStream, errorStream)
  })

  describe('#startContext', async () => {
    it('logs the name of the context right after a blank line', async () => {
      const context = new Context('a context', emptyFunction)

      consoleReporter.startContext(context)

      assert.deepStrictEqual(
        outStream.loggedString,
        `\n${context.name}\n`
      )
    })

    it('increase indentation when starting another context within the previous one', async () => {
      const context = new Context('a context', emptyFunction)
      const child = new Context('a child', emptyFunction)

      consoleReporter.startContext(context)
      consoleReporter.startContext(child)

      assertStringIncludes(
        outStream.loggedString,
        reindent`

          ${context.name}
            ${child.name}
        `
      )
    })

    context('with a skipped context', async () => {
      it('reports the context as skipped', async () => {
        const context = new Context('a context', emptyFunction, { skipped: true })

        consoleReporter.startContext(context)

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            \x1B[33m${context.name} (SKIPPED)
          `
        )
      })
    })
  })

  describe('#stopContext', async () => {
    it('decrease indentation related to the context', async () => {
      const context = new Context('a context', emptyFunction)
      const child = new Context('child context', emptyFunction)

      consoleReporter.startContext(context)
      consoleReporter.startContext(child)
      consoleReporter.stopContext()
      consoleReporter.startContext(child)

      assertStringIncludes(
        outStream.loggedString,
        reindent`
          ${context.name}
            ${child.name}
            ${child.name}
        `
      )
    })
  })

  describe('#stopTest', async () => {
    const described = new Context('a context', emptyFunction)

    beforeEach(async () => {
      consoleReporter.startContext(described)
    })

    it('logs the name of the last started test, indented, in green', async () => {
      const test = new Test('one test', emptyFunction)

      consoleReporter.startTest(test)
      consoleReporter.stopTest()

      assertStringIncludes(outStream.loggedString, `\x1B[32m  ${test.name}\x1B[0m`)
    })

    context('with an error', async () => {
      it('logs the name of the last started test, indented, in red, suffixed with " (FAILED)"', async () => {
        const test = new Test('one test', emptyFunction)

        consoleReporter.startTest(test)
        consoleReporter.stopTest(new Error("An error"))

        assertStringIncludes(errorStream.loggedString, `\x1B[31m  ${test.name} (FAILED)\x1B[0m`)
      })
    })

    context('with a skipped test', async () => {
      it('logs the name of the last started test, indented, in yellow, suffixed with " (SKIPPED)"', async () => {
        const test = new Test('one test', emptyFunction, { skipped: true })

        consoleReporter.startTest(test)
        consoleReporter.stopTest(new Error("skipped"))

        assertStringIncludes(errorStream.loggedString, `\x1B[33m  ${test.name} (SKIPPED)\x1B[0m`)
      })
    })
  })

  describe('#summarizeExecution', async () => {
    const summaryReporter = new SummaryReporter()

    beforeEach(async () => {
      summaryReporter.reset()
      consoleReporter = new ConsoleReporter(summaryReporter, outStream, errorStream)
    })

    it('summarize the execution using specific reporter', async () => {
      consoleReporter.startContext(new Context('a context', emptyFunction))
      consoleReporter.startTest(new Test('a test', emptyFunction))
      consoleReporter.stopTest(new Error("something bad happened"))
      consoleReporter.stopContext()
      consoleReporter.summarizeExecution()

      assert.ok(summaryReporter.startContextCalled)
      assert.ok(summaryReporter.stopContextCalled)
      assert.ok(summaryReporter.startTestCalled)
      assert.ok(summaryReporter.stopTestCalled)
      assert.ok(summaryReporter.summarizeExecutionCalled)
    })
  })
})
