import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  it,
} from '../../src/index.js'
import JunitReporter from '../../src/reporters/junit_reporter.js'
import Context from '../../src/runnables/context.js'
import Test from '../../src/runnables/test.js'
import TestStream from '../support/test_stream.js'
import { BenchmarkerStub, assertStringIncludes, assertStringDoesNotIncludes, emptyFunction } from '../support/helpers/index.js'
import { reindentTag as reindent } from 'reindent-template-literals'

describe('JunitReporter', async () => {
  const stream = new TestStream()
  const benchmarker = new BenchmarkerStub()

  let junitReporter: JunitReporter

  beforeEach(async () => {
    stream.reset()
    benchmarker.setIncrement(10)
    junitReporter = new JunitReporter(stream, benchmarker)
  })

  describe('#startContext', async () => {
    it('reports the xml header and <testsuites> root', async () => {
      junitReporter.startContext(new Context('', emptyFunction))

      assertStringIncludes(
        stream.loggedString,
        reindent`
          <?xml version="1.0" encoding="UTF-8" standalone="no"?>
          <testsuites>
        `
      )
    })

    it('reports the xml header one time only', async () => {
      junitReporter.startContext(new Context('', emptyFunction))
      junitReporter.startContext(new Context('', emptyFunction))

      assertStringDoesNotIncludes(
        stream.loggedString,
        reindent`
          <?xml version="1.0" encoding="UTF-8" standalone="no"?>
          <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        `
      )
    })

    it('reports the <testsuites> one time only', async () => {
      junitReporter.startContext(new Context('', emptyFunction))
      junitReporter.startContext(new Context('', emptyFunction))

      assert.equal(
        stream.loggedString.match(/<testsuites>/g)?.length,
        1
      )
    })

    it('closes a previously opened testsuite', async () => {
      const someTest = new Test('', emptyFunction)
      const someContext = new Context('some context', emptyFunction)
      const anotherContext = new Context('anotherContext', emptyFunction)

      someContext.addTest(someTest)
      someContext.addChildContext(anotherContext)

      junitReporter.startContext(someContext)
      junitReporter.startTest(someTest)
      junitReporter.stopTest()
      junitReporter.startContext(anotherContext)

      assertStringIncludes(
        stream.loggedString,
        '</testsuite>'
      )
    })
  })

  describe('#startTest', async () => {
    it("reports the test's context as a junit testsuite", async () => {
      const someTest = new Test('some test', emptyFunction)
      const testContext = new Context('test context', emptyFunction)

      testContext.addTest(someTest)

      junitReporter.startContext(testContext)
      junitReporter.startTest(someTest)

      assertStringIncludes(
        stream.loggedString,
        reindent`
          <testsuite name="test context" tests="1" timestamp="
        `
      )
    })

    it("reports the whole context hierarchy as a single testsuite", async () => {
      const someTest = new Test('some test', emptyFunction)
      const testContext = new Context('test context', emptyFunction)
      const childContext = new Context('the child', emptyFunction)

      testContext.addChildContext(childContext)
      childContext.addTest(someTest)

      junitReporter.startContext(testContext)
      junitReporter.startContext(childContext)
      junitReporter.startTest(someTest)

      assertStringIncludes(
        stream.loggedString,
        reindent`
          <testsuite name="test context, the child" tests="1" timestamp="
        `
      )
    })

    it("reports the number of tests in the context", async () => {
      const someTest = new Test('some test', emptyFunction)
      const anotherTest = new Test('another test', emptyFunction)
      const testContext = new Context('test context', emptyFunction)

      testContext.addTest(someTest)
      testContext.addTest(anotherTest)

      junitReporter.startContext(testContext)
      junitReporter.startTest(someTest)

      assertStringIncludes(
        stream.loggedString,
        reindent`
          <testsuite name="test context" tests="2" timestamp="
        `
      )
    })

    it("reports the test context only once", async () => {
      const someTest = new Test('some test', emptyFunction)
      const anotherTest = new Test('another test', emptyFunction)
      const testContext = new Context('test context', emptyFunction)

      testContext.addTest(someTest)
      testContext.addTest(anotherTest)

      junitReporter.startContext(testContext)
      junitReporter.startTest(someTest)
      junitReporter.stopTest()
      junitReporter.startTest(anotherTest)

      assert.equal(
        stream.loggedString.match(/<testsuite name="test context"/g)?.length,
        1
      )
    })
  })

  describe('#stopTest', async () => {
    it('reports a junit testcase', async () => {
      const someTest = new Test('some test', emptyFunction)
      const someContext = new Context('some context', emptyFunction)

      someContext.addTest(someTest)

      junitReporter.startContext(someContext)
      junitReporter.startTest(someTest)
      junitReporter.stopTest(undefined)

      assertStringIncludes(
        stream.loggedString,
        '    <testcase name="some test" classname="some context" time="0.01">\n    </testcase>'
      )
    })

    it('reports failures', async () => {
      const someTest = new Test('some test', emptyFunction)
      const someContext = new Context('some context', emptyFunction)

      someContext.addTest(someTest)

      benchmarker.setIncrement(1542.34)

      junitReporter.startContext(someContext)
      junitReporter.startTest(someTest)
      junitReporter.stopTest(new Error('an <"error">'))

      assertStringIncludes(
        stream.loggedString,
        reindent`
          <testcase name="some test" classname="some context" time="1.54234">
                <failure message="an &lt;&quot;error&quot;>" type="Error">
                </failure>
              </testcase>
        `
      )
    })
  })

  describe('#stopContext', async () => {
    it('closes the junit testsuite', async () => {
      const someTest = new Test('', emptyFunction)
      const someContext = new Context('some context', emptyFunction)

      someContext.addTest(someTest)

      junitReporter.startContext(someContext)
      junitReporter.startTest(someTest)
      junitReporter.stopTest()
      junitReporter.stopContext()

      assertStringIncludes(
        stream.loggedString,
        '</testsuite>'
      )
    })

    it('closes the junit testsuite only if it has been opened before', async () => {
      const someContext = new Context('some context', emptyFunction)

      junitReporter.startContext(someContext)
      junitReporter.stopContext()

      assertStringDoesNotIncludes(stream.loggedString, '</testsuite>')
    })
  })

  describe('#summarizeExecution', async () => {
    it('closes the testsuites node', async () => {
      junitReporter.startContext(new Context('', emptyFunction))
      junitReporter.stopContext()
      junitReporter.summarizeExecution()

      assertStringIncludes(stream.loggedString, '</testsuites>')
    })

    it('closes the testsuites node only if it has been opened before', async () => {
      junitReporter.summarizeExecution()

      assertStringDoesNotIncludes(stream.loggedString, '</testsuites>')
    })
  })
})
