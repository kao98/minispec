import {
  beforeEach,
  describe,
  it,
} from '../../src/index.js'
import BenchmarkReporter from '../../src/reporters/benchmark_reporter.js'
import TestStream from '../support/test_stream.js'
import { BenchmarkerStub, assertStringIncludes, emptyFunction } from '../support/helpers/index.js'
import Test from '../../src/runnables/test.js'
import Context from '../../src/runnables/context.js'

describe('BenchmarkReporter', async () => {
  const outStream = new TestStream()
  const errorStream = new TestStream()

  const testBenchmarker = new BenchmarkerStub()
  const contextBenchmarker = new BenchmarkerStub()
  const contextBenchmarkerFactory = () => contextBenchmarker

  let benchmarkReporter: BenchmarkReporter

  beforeEach(async () => {
    outStream.reset()
    errorStream.reset()

    testBenchmarker.setIncrement(10)
    contextBenchmarker.setIncrement(20)

    benchmarkReporter = new BenchmarkReporter(
      undefined,
      outStream,
      errorStream,
      testBenchmarker,
      contextBenchmarkerFactory
    )
  })

  describe('#stopTest', async () => {
    it('reports the execution time of the test', async () => {
      const test = new Test('a test', emptyFunction)

      benchmarkReporter.startTest(test)
      benchmarkReporter.stopTest(undefined)

      assertStringIncludes(
        outStream.loggedString,
        '  ⏱️  10 milliseconds'
      )
    })

    it('reports the execution time of a context', async () => {
      const context = new Context('a context', emptyFunction)
      const test = new Test('a test', emptyFunction)

      benchmarkReporter.startContext(context)
      benchmarkReporter.startTest(test)
      benchmarkReporter.stopTest(undefined)
      benchmarkReporter.stopContext()

      assertStringIncludes(
        outStream.loggedString,
        `\x1b[90m${context.name}:\x1b[0m ⏱️  20 milliseconds`
      )
    })
  })
})
