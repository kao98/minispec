import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  context,
  it,
} from '../../src/index.js'
import BaseReporter from '../../src/reporters/base_reporter.js'
import Context from '../../src/runnables/context.js'
import Test from '../../src/runnables/test.js'
import { emptyFunction } from '../support/helpers/index.js'

describe('BaseReporter', async () => {
  let reporter: BaseReporter

  beforeEach(async () => {
    reporter = new BaseReporter()
  })

  describe('#startContext', async () => {
    context('when a test is active', async () => {
      it('raises an error with message "test in progress"', async () => {
        const context = new Context('a context', emptyFunction)
        const test = new Test('a test', emptyFunction)

        reporter.startContext(context)
        reporter.startTest(test)

        assert.throws(
          () => {
            reporter.startContext(new Context('another context', emptyFunction))
          },
          {
            message: 'Test in progress, stop it before starting a new context'
          }
        )
      })
    })
  })

  describe('#stopContext', async () => {
    context('when no context is active', async () => {
      it('throws an error with message "no active context"', async () => {
        const context = new Context('a context', emptyFunction)

        reporter.startContext(context)
        reporter.stopContext()

        assert.throws(
          () => {
            reporter.stopContext()
          },
          {
            message: 'No active context to stop'
          }
        )
      })
    })

    context('when a test is active', async () => {
      it('raises an error with message "test in progress"', async () => {
        const context = new Context('a context', emptyFunction)
        const test = new Test('a test', emptyFunction)

        reporter.startContext(context)
        reporter.startTest(test)

        assert.throws(
          () => {
            reporter.stopContext()
          },
          {
            message: 'Test in progress, stop it before stopping active context'
          }
        )
      })
    })
  })

  describe('#startTest', async () => {
    context('when a test is already active', async () => {
      it('raises an error with message "test in progress"', async () => {
        const context = new Context('a context', emptyFunction)
        const test = new Test('a test', emptyFunction)

        reporter.startContext(context)
        reporter.startTest(test)

        assert.throws(
          () => {
            reporter.startTest(new Test('another test', emptyFunction))
          },
          {
            message: 'Test in progress, stop it before starting a new one'
          }
        )
      })
    })
  })

  describe('#stopTest', async () => {
    const described = new Context('a context', emptyFunction)

    beforeEach(async () => {
      reporter.startContext(described)
    })

    context('when there is no active test', async () => {
      it('throws an error with message "no active test to stop"', async () => {
        const test = new Test('one test', emptyFunction)

        reporter.startTest(test)
        reporter.stopTest()

        assert.throws(
          () => {
            reporter.stopTest()
          },
          {
            message: 'No active test to stop'
          }
        )
      })
    })
  })

  describe('#summarizeExecution', async () => {
    context('when a test has not been closed', async () => {
      it('raises an error with message "test in progress"', async () => {
        const context = new Context('a context', emptyFunction)
        const test = new Test('a test', emptyFunction)

        reporter.startContext(context)
        reporter.startTest(test)

        assert.throws(
          () => {
            reporter.summarizeExecution()
          },
          {
            message: 'Test in progress, stop it before asking for execution summary'
          }
        )
      })
    })

    context('when a context has not been closed', async () => {
      it('raises an error with message "context in progress"', async () => {
        const context = new Context('a context', emptyFunction)

        reporter.startContext(context)

        assert.throws(
          () => {
            reporter.summarizeExecution()
          },
          {
            message: 'Context in progress, stop it before asking for execution summary'
          }
        )
      })
    })
  })
})
