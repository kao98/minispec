import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  context,
  it,
} from '../../src/index.js'
import SummaryReporter from '../../src/reporters/summary_reporter.js'
import Test from '../../src/runnables/test.js'
import TestStream from '../support/test_stream.js'
import { assertStringIncludes, emptyFunction } from '../support/helpers/index.js'
import { reindentTag as reindent } from 'reindent-template-literals'

describe('SummaryReporter', async () => {
  const outStream = new TestStream()
  const errorStream = new TestStream()

  let summaryReporter: SummaryReporter

  beforeEach(async () => {
    outStream.reset()
    errorStream.reset()
    summaryReporter = new SummaryReporter(outStream, errorStream)
  })

  describe('#summarizeExecution', async () => {
    context('when no test has been executed', async () => {
      it('reports "Finished - No test has been executed"', async () => {
        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[33mNo test has been executed 🤔\x1B[0m
          `
        )
      })
    })

    context('with no failure', async () => {
      it('reports "Finished - No failure"', async () => {
        const test = new Test('a test', emptyFunction)

        summaryReporter.startTest(test)
        summaryReporter.stopTest()

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[32m1 test, no failure 👏\x1B[0m
          `
        )
      })

      it('pluralizes the number of tests', async () => {
        const test = new Test('a test', emptyFunction)

        summaryReporter.startTest(test)
        summaryReporter.stopTest()
        summaryReporter.startTest(test)
        summaryReporter.stopTest()

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[32m2 tests, no failure 👏\x1B[0m
          `
        )
      })
    })

    context('with failures', async () => {
      it('reports the failures', async () => {
        const test = new Test('a test', emptyFunction)
        const test2 = new Test('another test', emptyFunction)

        const error1 = new Error("something bad happened")
        const error2 = new Error("something else happened")

        error1.stack = "at Spec (file:///somewhere-1)"
        error2.stack = "at File (file:///somewhere-2)"

        summaryReporter.startTest(test)
        summaryReporter.stopTest(error1)
        summaryReporter.startTest(test2)
        summaryReporter.stopTest(error2)

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          errorStream.loggedString,
          reindent`
            Failures:

              1) , it ${test.name}
                 \x1B[31msomething bad happened\x1b[0m
                 \x1b[32m# Spec (file:///somewhere-1)\x1b[0m


              2) , it ${test2.name}
                 \x1B[31msomething else happened\x1b[0m
                 \x1b[32m# File (file:///somewhere-2)\x1b[0m

          `
        )
      })

      context('with an assertion error', async () => {
        it('reports expected and actual properties', async () => {
          const test = new Test('a test', emptyFunction)

          let assertionError
          try {
            assert.equal(1, 2)
          } catch (error) {
            assertionError = error
          }

          summaryReporter.startTest(test)
          summaryReporter.stopTest(assertionError)

          summaryReporter.summarizeExecution()

          assertStringIncludes(
            errorStream.loggedString,
            reindent`
              Failures:

                1) , it ${test.name}
                   \x1B[31mExpected values to be strictly equal:

                   1 !== 2
                   \x1b[0m

              \x1B[31m       expected: 2\x1B[0m
              \x1B[31m            got: 1\x1B[0m
            `
          )
        })

        context('with a negated assertion', async () => {
          it('reports expected being not expected', async () => {
            const test = new Test('a test', emptyFunction)

            summaryReporter.startTest(test)

            try {
              assert.notEqual(1, 1)
            } catch (error) {
              summaryReporter.stopTest(error)
            }

            summaryReporter.summarizeExecution()

            assertStringIncludes(
              errorStream.loggedString,
              reindent`
                Failures:

                  1) , it ${test.name}
                     \x1B[31mExpected "actual" to be strictly unequal to: 1\x1B[0m

                \x1B[31m       expected: not 1\x1B[0m
                \x1B[31m            got: 1\x1B[0m
              `
            )
          })
        })
      })

      it('reports the number of failure', async () => {
        const test = new Test('a test', emptyFunction)

        summaryReporter.startTest(test)
        summaryReporter.stopTest(new Error("something bad happened"))

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[31m1 test, 1 failure\x1B[0m
          `
        )
      })

      it('pluralizes the number of failures', async () => {
        const test = new Test('a test', emptyFunction)

        summaryReporter.startTest(test)
        summaryReporter.stopTest(new Error("something bad happened"))
        summaryReporter.startTest(test)
        summaryReporter.stopTest(new Error("something else happened"))

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[31m2 tests, 2 failures\x1B[0m
          `
        )
      })

      it('lists the error locations', async () => {
        const test = new Test('a test', emptyFunction)

        const error1 = new Error("something bad happened")
        const error2 = new Error("something else happened")

        error1.stack = "at file:///somewhere-1:1:2"
        error2.stack = "at file:///somewhere-2:3:4"

        summaryReporter.startTest(test)
        summaryReporter.stopTest(error1)
        summaryReporter.startTest(test)
        summaryReporter.stopTest(error2)

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          errorStream.loggedString,
          reindent`

            Spec with failures:

            \x1b[31mfile:///somewhere-1:1:2\x1b[0m
            \x1b[31mfile:///somewhere-2:3:4\x1b[0m
          `
        )
      })
    })

    context('with skipped', async () => {
      it('reports the skipped tests', async () => {
        const test = new Test('a test', emptyFunction, { skipped: true, location: 'location #1' })
        const test2 = new Test('another test', emptyFunction, { skipped: true, location: 'location #2' })

        const error1 = new Error("skipped")
        const error2 = new Error("skipped")

        summaryReporter.startTest(test)
        summaryReporter.stopTest(error1)
        summaryReporter.startTest(test2)
        summaryReporter.stopTest(error2)

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          errorStream.loggedString,
          reindent`
            Skipped:

              1) \x1B[33m, it ${test.name}\x1B[0m
                 \x1B[33m# location #1\x1B[0m


              2) \x1B[33m, it ${test2.name}\x1B[0m
                 \x1B[33m# location #2\x1B[0m

          `
        )
      })

      it('reports the number of skipped tests', async () => {
        const test = new Test('a test', emptyFunction, { skipped: true })

        summaryReporter.startTest(test)
        summaryReporter.stopTest(new Error("skipped"))
        summaryReporter.startTest(test)
        summaryReporter.stopTest()

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[32m1 test (1 skipped), no failure 👏\x1B[0m
          `
        )
      })

      it('reports "no test executed" when all tests are skipped', async () => {
        const test = new Test('a test', emptyFunction, { skipped: true })

        summaryReporter.startTest(test)
        summaryReporter.stopTest(new Error("skipped"))

        summaryReporter.summarizeExecution()

        assertStringIncludes(
          outStream.loggedString,
          reindent`

            Finished
            \x1B[33mNo test has been executed (1 skipped) 🤔\x1B[0m
          `
        )
      })
    })

    context('with benchmarking data', async () => {
      beforeEach(async () => {
        summaryReporter.startTimer(0)
        summaryReporter.addTimerInterval(10, 'discovering')
        summaryReporter.addTimerInterval(20, 'execution')
        summaryReporter.stopTimer(30)

        summaryReporter.summarizeExecution()
      })

      it('reports the total duration', async () => {
        assertStringIncludes(
          outStream.loggedString,
          'Finished in 30 milliseconds'
        )
      })

      it('reports the interval durations', async () => {
        assertStringIncludes(
          outStream.loggedString,
          '(discovering took 10 milliseconds, execution took 10 milliseconds)'
        )
      })
    })
  })
})
