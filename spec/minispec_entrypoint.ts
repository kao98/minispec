import MiniSpec from '../src/index.js'
import { BaseReporter, SummaryReporter, ConsoleReporter, JunitReporter } from '../src/reporters/index.js'
import fs from 'fs'

import './api/it_spec.js'
import './api/context_spec.js'
import './api/describe_spec.js'
import './reporters/base_reporter_spec.js'
import './reporters/benchmark_reporter_spec.js'
import './reporters/console_reporter_spec.js'
import './reporters/junit_reporter_spec.js'
import './reporters/summary_reporter_spec.js'
import './engine/active_context_spec.js'
import './runnables/context_spec.js'
import './runnables/test_spec.js'

const reporters: BaseReporter[] = []

if (process.env['CI']) {
  const junitReport = fs.createWriteStream('./junitReport.xml')

  reporters.push(new SummaryReporter())
  reporters.push(new JunitReporter(junitReport))
} else {
  reporters.push(new ConsoleReporter())
}

MiniSpec
  .getInstance()
  .setReporters(reporters)
  .execute()
