import assert from 'assert/strict'
import {
  beforeEach,
  describe,
  context,
  it
} from '../../src/index.js'
import ActiveContext from '../../src/engine/active_context.js'
import EngineMock from '../support/engine_mock.js'
import { emptyFunction } from '../support/helpers/index.js'
import Context from '../../src/runnables/context.js'

describe('ActiveContext', async () => {
  const engine = new EngineMock()
  let activeContext: ActiveContext

  beforeEach(async () => {
    activeContext = new ActiveContext(engine)
    engine.activeContext = activeContext
  })

  describe('#hasOne', async () => {
    context('with an active context', async () => {
      beforeEach(async () => {
        activeContext.set(new Context('a context', emptyFunction))
      })

      it('returns true', async () => assert.ok(activeContext.hasOne()))
    })

    context('without an active context', async () => {
      it('returns false', async () => assert.strictEqual(activeContext.hasOne(), false))
    })
  })

  describe('#get activeContext', async () => {
    context('with no active context', async () => {
      it('creates a new context', async () => {
        assert.strictEqual(activeContext.hasOne(), false)
        assert.notEqual(activeContext.get(), null)
        assert.strictEqual(activeContext.get().constructor.name, Context.name)
      })
    })
  })
})
